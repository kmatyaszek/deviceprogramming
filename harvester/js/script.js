/**
ProgramTV.Interia.PL Harvester
Coding: Adam Kopeć <adam.kopec@gmail.com>

Script reads pages, converts them to the JSON format and sends to a server-side disk saving script.
**/
function round(n) {
    return Math.round(n*100+((n*1000)%10>4?1:0))/100;
}

function number(input)
{
    return parseFloat(input.replace(/[^0123456789,.]/, '').replace(/,/,'.'));
}

var $result = $('#result');
var $info = $('#info');
var $pb = $('#progress');
var $btn = $('button');

var err = false;

var today = new Date();
var todayStr = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
var tomorrow = today;
tomorrow.setDate(tomorrow.getDate()+1);
var tomorrowStr = tomorrow.getFullYear() + '-' + (tomorrow.getMonth() + 1) + '-' + tomorrow.getDate();

function extractStations(html)
{
    var stations = [];

    var $lis = $('ul#channelList li a', $(html));
    var cnt = $lis.length;
    
    $info.text('Pobieranie danych ' + cnt + ' stacji...');
    
    $lis.each(function(i) {
        var station = { 'url': $(this).attr('href'), 'name': $(this).text(), 'id' : (i+1) };
        //ex: wszystkie/tvp-1,17024926,10?sDate=2011-11-20&sTimeF=18&sTimeT=4&sTimeT=4&cn=&p=1
        //chcemy: sTimeF=5&sTimeT=4
        station.url = station.url.replace(/sTimeF=\d+/, 'sTimeF=5').replace(/sTimeT=\d+/, 'sTimeT=4');
        station.url = 'http://programtv.interia.pl/kanaly/' + station.url;
        //console.log(station.url);
        stations.push(station);
        $pb.progressbar('option', 'value', Math.floor((i+1) / cnt * 100));
    });
    
    $info.text('Pobrano dane ' + cnt + ' stacji.');
    
    return stations;
}

function saveStations(stations)
{
    var ok = false;
    $.ajax({
        url: 'stations.php',
        async: false,
        type: 'POST',
        dataType: 'json',
        data: { 'stations': Base64.encode(JSON.stringify(stations))},
        success: function(content) {
            ok = content.success;
        }
    });
    
    return ok;
}

function extractProgram($tr, fullInfo)
{
    var type = $.trim($('td span.type', $tr).text());
    if (type.substr(0,2) == '- ')
        type = type.substring(2);
        
    var category = $tr.attr('class');
    switch(category)
    {
        case 't2':
            category = 'Dla dzieci';
            break;
        case 't3':
            category = 'Film';
            break;
        case 't4':
            category = 'Informacje';
            break;
        case 't5':
            category = 'Sport';
            break;
        case 't6':
            category = 'Rozrywka';
            break;
        case 't7':
            category = 'Dokument';
            break;
        case 't8':
            category = 'Talkshow';
            break;
        default:
            category = 'Wszystkie';
            break;
    }
    
    var url = 'http://programtv.interia.pl' + $('td a', $tr).attr('href');
    
    var dfull = '';
    if (fullInfo)
    {
        if (category == 'Film')
        {
            $.ajax({
                url: 'proxy.php',
                async: false,
                data: { 'url': Base64.encode(url)},
                success: function(content) {
                    dfull = $('#article .program', $(content)).html();
                }
            });
        }
    }
    
    return {
        'hour' : $('td.hour', $tr).text(),
        'name' : $('td a strong', $tr).text(),
        'type' : type,
        'short' : $('div.desc', $tr).html(),
        'category' : category,
        'url' : url,
        'full' : dfull
    }
}

function savePrograms(programs, station, date)
{
    var ok = false;
    $.ajax({
        url: 'station.php',
        async: false,
        type: 'POST',
        dataType: 'json',
        data: { 'programs': Base64.encode(JSON.stringify(programs)), 'id' : station.id, 'date' : date },
        success: function(content) {
            ok = content.success;
        }
    });
    
    return ok;
}

function processStations(stations, date)
{
    $pb.progressbar('option', 'value', 0);
    var cnt = stations.length;
    
    if (date === undefined)
    {
        date = todayStr;
    }
    
    $.each(stations, function(i, station) {
        $info.text('(' + (i+1) + '/' + stations.length + ') Przetwarzanie programu stacji ' + station.name + ' na dzień ' + date + '...');
        
        station.url = station.url.replace(/sDate=[^&]+/, 'sDate=' + date)
        
        $.ajax({
            url: 'proxy.php',
            async: false,
            data: { 'url': Base64.encode(station.url)},
            success: function(content) {
                var $trs = $('table.channelCont tr', $(content));
                var programs = [];
                $trs.each(function(i) {
                    var program = extractProgram($(this), true);
                    programs.push(program);
                });
                
                savePrograms(programs, station, date);
            }
        });
        
        $pb.progressbar('option', 'value', round((i+1) / cnt) * 100);
    });
    
    $info.text('Stacje przetworzono.');
}

function finished()
{
    if (!err)
        $info.text('Zakończono.');
        
    $pb.progressbar('option', 'value', 0);
    $btn.button( "option", "label", "Start");
    $btn.button( "option", "disabled", false);
}

function start()
{
    $btn.click(function() {
        stop = true;
    });
    
    var now = new Date();
    var hours = now.getHours()
    var minutes = now.getMinutes()
    if (minutes < 10){
        minutes = "0" + minutes
    }
    
    $btn.button( "option", "label", "Rozpoczęto o " + hours + ":" + minutes + "...");
    $btn.button( "option", "disabled", true);
    stop = false;
    $.ajax({
        url: 'proxy.php',
        async: false,
        data: { 'url': Base64.encode('http://programtv.interia.pl/kanaly/wszystkie,10')},
        success: function(content) {
            var stations = extractStations(content);
            if (saveStations(stations))
            {
                processStations(stations);
                processStations(stations, tomorrowStr);
            }
            else
            {
                err = true;
                $info.text('Nie udało się zapisać na serwerze listy stacji.');
            }
            
            finished();
        }
    });
}

$(document).ready(function() {
    $btn.button();
    $pb.progressbar();
    
    $btn.click(function() {
        start();
    });
});