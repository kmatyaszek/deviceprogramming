﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using ICSharpCode.SharpZipLib.Zip;
using System.IO.IsolatedStorage;
using ICSharpCode.SharpZipLib.Core;
using System.IO;

namespace ProgramTV
{
    public class CompressHelper
    {
        private void CompressFolder(string path, ZipOutputStream zipStream, int folderOffset, IsolatedStorageFile isf)
        {
            string[] files = isf.GetFileNames(System.IO.Path.Combine(path, "*.*"));

            foreach (string filename in files)
            {
                string filenameWithPath = System.IO.Path.Combine(path, filename);
                string entryName = filenameWithPath.Substring(folderOffset); // Makes the name in zip based on the folder
                entryName = ZipEntry.CleanName(entryName); // Removes drive from name and fixes slash direction
                ZipEntry newEntry = new ZipEntry(entryName);
                newEntry.DateTime = isf.GetLastWriteTime(filenameWithPath).DateTime; // Note the zip format stores 2 second granularity

                // To permit the zip to be unpacked by built-in extractor in WinXP and Server2003, WinZip 8, Java, and other older code,
                // you need to do one of the following: Specify UseZip64.Off, or set the Size.
                // If the file may be bigger than 4GB, or you do not need WinXP built-in compatibility, you do not need either,
                // but the zip will be in Zip64 format which not all utilities can understand.
                //   zipStream.UseZip64 = UseZip64.Off;
                using (IsolatedStorageFileStream stream = new IsolatedStorageFileStream(filenameWithPath, System.IO.FileMode.Open, isf))
                {
                    newEntry.Size = stream.Length;
                }

                zipStream.PutNextEntry(newEntry);

                // Zip the file in buffered chunks
                // the "using" will close the stream even if an exception occurs
                byte[] buffer = new byte[4096];
                using (IsolatedStorageFileStream streamReader = isf.OpenFile(filenameWithPath, System.IO.FileMode.Open))
                {
                    StreamUtils.Copy(streamReader, zipStream, buffer);
                }
                zipStream.CloseEntry();
            }
            string[] folders = isf.GetDirectoryNames(System.IO.Path.Combine(path, "*.*"));
            foreach (string folder in folders)
            {
                CompressFolder(System.IO.Path.Combine(path, folder), zipStream, folderOffset, isf);
            }
        }

        public void CreateZip(string outPathname, string password, string folderName)
        {

            using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (IsolatedStorageFileStream fsOut = new IsolatedStorageFileStream(outPathname, System.IO.FileMode.Create, isf))
                {
                    ZipOutputStream zipStream = new ZipOutputStream(fsOut);

                    zipStream.SetLevel(3); //0-9, 9 being the highest level of compression

                    zipStream.Password = password;  // optional. Null is the same as not setting.

                    // This setting will strip the leading part of the folder path in the entries, to
                    // make the entries relative to the starting folder.
                    // To include the full path for each entry up to the drive root, assign folderOffset = 0.

                    // int folderOffset = folderName.Length + (folderName.EndsWith("\\") ? 0 : 1); // hu: currently not used for WP7 sample
                    int folderOffset = 0;

                    CompressFolder(folderName, zipStream, folderOffset, isf);

                    zipStream.Close();
                }
            }
        }

        public static void UnZip(string fileName, string date)
        {
            IsolatedStorageFile myStore = IsolatedStorageFile.GetUserStoreForApplication();
            ZipEntry entry;
            using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
            using (ZipInputStream zis = new ZipInputStream(new IsolatedStorageFileStream(fileName,FileMode.Open,isf)))            
            {
                string folderForOutput = "Programs"; //System.IO.Path.GetFileNameWithoutExtension(fileName);

                if (!isf.DirectoryExists(folderForOutput))
                {
                    isf.CreateDirectory(folderForOutput);
                }
                
                
                if (myStore.DirectoryExists(folderForOutput+"/"+date) == false)
                {
                    myStore.CreateDirectory(folderForOutput + "/" + date);
                }
                
                while ((entry = zis.GetNextEntry()) != null)
                {
                    if (isf.FileExists(entry.Name))
                    {
                        isf.DeleteFile(entry.Name);
                    }
                    long sizeToWrite = entry.Size;
                    using (FileStream fs = isf.CreateFile(folderForOutput + "/" + date + "/" + entry.Name))
                                      
                    using (BinaryWriter sw = new BinaryWriter(fs))
                    {
                        byte[] data = new byte[sizeToWrite];
                        int bytesRead = zis.Read(data, 0, data.Length);
                        if (bytesRead > 0)
                        {
                            sw.Write(data);
                        }
                    }
                }
            }
        }
    }
}
