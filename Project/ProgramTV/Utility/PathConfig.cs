﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO.IsolatedStorage;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ProgramTV.Entity;

namespace ProgramTV.Utility
{
    public static class PathConfig
    {
        static IsolatedStorageFile myStore = null;

        static PathConfig()
        {
            try
            {
                myStore = IsolatedStorageFile.GetUserStoreForApplication();

                if (myStore.DirectoryExists("Downlaod") == false)
                {
                    myStore.CreateDirectory("Donwload");
                }

                if (myStore.DirectoryExists("Stations") == false)
                {
                    myStore.CreateDirectory("Stations");
                    PathStations = "Stations";
                }

                if (myStore.DirectoryExists("Programs") == false)
                {
                    myStore.CreateDirectory("Programs");
                    PathPrograms = "Programs";
                }
            }
            catch (Exception ex)
            {
                throw (new Exception("Constructor problem.", ex));
            }
        }

        public static void createFile(string name)
        {
            try
            {
                using (var isoFileStream = new IsolatedStorageFileStream(System.IO.Path.Combine(PathFiles, name), FileMode.OpenOrCreate, myStore))
                {

                    using (var isoFileWriter = new StreamWriter(isoFileStream))
                    {
                        isoFileWriter.WriteLine("password: 1234qwer");
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new Exception("Problem ze stworzeniem pliku.", ex));
            }
        }

        public static void openFile(string name)
        {
            try
            {
                string tmp;
                using (var isoFileStream = new IsolatedStorageFileStream(System.IO.Path.Combine(PathFiles, "pass.txt"), FileMode.Open, myStore))
                {

                    using (var isoFileReader = new StreamReader(isoFileStream))
                    {
                        tmp = isoFileReader.ReadLine();
                    }
                }

                MessageBox.Show(tmp);
            }
            catch (Exception ex)
            {
                throw (new Exception("Problem z odczytem pliku.", ex));
            }
        }

        public static string getFilesFromDownloadFolder()
        {
            StringBuilder sb = new StringBuilder();
            string[] tmp = myStore.GetDirectoryNames();
            string[] files = myStore.GetFileNames();
            foreach (var item in files)
            {
                sb.Append(item + "\n");
            }
            foreach (var item in tmp)
            {
                sb.Append(item + "\n");
            }

            return sb.ToString();
        }

        public static List<string> getFolders()
        {
            using (var store = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (store.DirectoryExists("Programs") == true)
                {
                    List<string> result = new List<string>();

                    string[] directories = myStore.GetDirectoryNames("Programs/*");

                    foreach (var item in directories)
                    {
                        result.Add(item);
                    }

                    if (result.Count != 0)
                        return result;
                    else
                        return null;
                }
                else
                {
                    return null;
                }
            }           
        }

        public static bool checkFolder(string date)
        {
            using (var store = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (store.DirectoryExists("Programs/" + date) == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static void deleteFolder(string nazwa)
        {
            using (var store = IsolatedStorageFile.GetUserStoreForApplication())
            {
              
                if (store.DirectoryExists("Programs") == true)
                {                  
                    List<string> result = new List<string>();
                    string[] directories = myStore.GetDirectoryNames("Programs/*");
               
                    foreach (var item in directories)
                    {
                        if (nazwa == item)
                        {
                            string a = "";
                            string[] directories2=myStore.GetFileNames("Programs/"+item+"/*");
                            foreach(var item2 in directories2)
                            {
                                myStore.DeleteFile("Programs/"+item+"/"+item2);
                             
                            }
                            
                            myStore.DeleteDirectory("Programs/"+item);                     
                  
                        }
                    }
                }
            }
        }

        public static void DeleteHistoryFile()
        {
            try
            {
                using (var store = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    if (store.FileExists("ProgramsReminder.store") == true)
                    {
                        store.DeleteFile("ProgramsReminder.store");
                        MessageBox.Show("Historia została wyczyszczona.", "Operacja zakończona powodzeniem.", MessageBoxButton.OK);
                    }
                    else
                    {
                        MessageBox.Show("Plik z historią nie istnieje.", "Operacja zakończona niepowodzeniem.", MessageBoxButton.OK);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem z usunięciem pliku.\n\n" + ex.Message, "Błąd.", MessageBoxButton.OK);
            }
        }

        public static void SerializeData()
        {
            try
            {
                IsolatedStorageFile myStore = IsolatedStorageFile.GetUserStoreForApplication();
                using (IsolatedStorageFileStream fs = new IsolatedStorageFileStream(
                            "ProgramsReminder.store", FileMode.Create, myStore))
                {
                    JSONNETSerializationHelper.Serialize(fs, ProcessInspector.Instance.ProgramsReminder);
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem z zapisem danych.\n\n" + ex.Message, "Błąd.", MessageBoxButton.OK);
            }
        }

        public static void DeserializeData()
        {
            try
            {
                IsolatedStorageFile myStore = IsolatedStorageFile.GetUserStoreForApplication();
                if (myStore.FileExists("ProgramsReminder.store") == true)
                {
                    using (IsolatedStorageFileStream fs = new IsolatedStorageFileStream(
                             "ProgramsReminder.store", FileMode.Open, myStore))
                    {
                        ProcessInspector.Instance.ProgramsReminder = (ObservableCollection<ProgramReminder>)JSONNETSerializationHelper.Deserialize(fs, typeof(ObservableCollection<ProgramReminder>));
                    };
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem z odczytem danych.\n\n" + ex.Message, "Błąd.", MessageBoxButton.OK);
            }
        }

        public static string PathStorage { get; set; }
        public static string PathDownload { get; set; }
        public static string PathFiles { get; set; }
        public static string PathPrograms { get; set; }
        public static string PathStations { get; set; }
    }
}
