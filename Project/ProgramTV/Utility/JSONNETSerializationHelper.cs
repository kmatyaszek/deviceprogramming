﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO;
using Newtonsoft.Json;

namespace ProgramTV.Utility
{
    public class JSONNETSerializationHelper
    {
        public static void Serialize(Stream streamObject, object obj)
        {
            if (obj == null || streamObject == null)
                return;

            JsonSerializer ser = new JsonSerializer();
            JsonWriter jw = new JsonTextWriter(new StreamWriter(streamObject));
            ser.Serialize(jw, obj);
            jw.Flush();
        }

        public static object Deserialize(Stream streamObject, Type objectType)
        {
            if (objectType == null || streamObject == null)
                return null;

            JsonSerializer ser = new JsonSerializer();
            JsonReader jr = new JsonTextReader(new StreamReader(streamObject));
            return ser.Deserialize(jr, objectType);
        }
    }
}
