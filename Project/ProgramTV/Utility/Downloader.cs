﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProgramTV
{
    public class Downloader
    {
        protected Observer observer;
        /// <summary>
        /// Zwraca obiekt Table wypełniony danymi
        /// </summary>
        public Table getTable(DateTime forDate, Observer observer)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Zwraca ścieżkę do pobranego pliku
        /// </summary>
        protected string getTableData()
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Zwraca lokalizację wypakowanych danych
        /// </summary>
        protected string extractData(string archivePath)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Tworzy obiekt Table z danych z plików
        /// </summary>
        protected Table buildTable(string dataPath)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Sprząta, czyli wyrzuca niepotrzebne pliki
        /// </summary>
        protected void cleanUp()
        {
            throw new System.NotImplementedException();
        }
    }
}
