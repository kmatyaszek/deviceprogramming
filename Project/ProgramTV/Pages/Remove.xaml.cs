﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using ProgramTV.Utility;
using System.IO.IsolatedStorage;
using System.Threading;

namespace ProgramTV.Pages
{
    public partial class Remove : PhoneApplicationPage
    {
        public Remove()
        {
            InitializeComponent();
            IsolatedStorageFile myStore = IsolatedStorageFile.GetUserStoreForApplication();            
            listBox1.ItemsSource = PathConfig.getFolders();
        }

        private void ButtonRemoveProgram_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string nazwa = listBox1.SelectedItem.ToString();
                PathConfig.deleteFolder(nazwa);
                listBox1.ItemsSource = PathConfig.getFolders();
                MessageBox.Show("Pogram został poprawnie usunięty.", "Operacja zakończona powodzeniem.", MessageBoxButton.OK);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas usuwania programu.\n\n" + ex.Message, "Błąd.", MessageBoxButton.OK);
            }
        }

        private void ButtonLoadProgram_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (listBox1.SelectedItem != null)
                {
                    ProcessInspector.Instance.ActualDate = listBox1.SelectedItem as string;
                    NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas wczytywania programu.\n\n" + ex.Message, "Błąd.", MessageBoxButton.OK);
            }
        }
    }
}