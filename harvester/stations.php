<?php
require "lib/XmlSerializer.php";

$stations = json_decode(base64_decode($_POST['stations']), true);

$fp = fopen("data/stations.xml", "w");
fwrite($fp, XMLSerializer::generateValidXmlFromArray($stations, 'stations', 'station'));
fclose($fp);

header('Content-Type: application/json');
echo json_encode(array('success' => true));