﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using System.Windows.Data;
using System.Globalization;

namespace ProgramTV.Coverters
{
    public class DownloadConverter : IValueConverter
    {
        public object Convert(object value,Type targetType,object parameter,CultureInfo culture)
        {
           //return visibility ? Visibility.Visible : Visibility.Collapsed;

            int status = int.Parse(value.ToString());
            if (status == 1)
                return true;
            else
                return false;
        }

        public object ConvertBack(object value,Type targetType,object parameter,CultureInfo culture)
        {
            Visibility visibility = (Visibility)value;
            return (visibility == Visibility.Visible);
        }
     

    }
}
