﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Tasks;
using System.Collections.Generic;

namespace ProgramTV.Utility
{
    public class DeleteHelper
    {
        public static void DeletePrograms()
        {
            int days = int.Parse(SettingsHelper.getValue("numOfDays"));
            DateTime deleteDate = DateTime.Now.AddDays(-days);

            List<string> folders = PathConfig.getFolders();

            if (folders != null)
            {
                foreach (var item in folders)
                {
                    DateTime folderDate = DateTime.Parse(item);
                    if (folderDate < deleteDate)
                        PathConfig.deleteFolder(item);
                }
            }
        }
    }
}
