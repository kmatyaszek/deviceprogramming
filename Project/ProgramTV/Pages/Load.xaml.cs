﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Scheduler;
using ProgramTV.Entity;
using ProgramTV.Utility;

namespace ProgramTV.Pages
{
    public partial class Load : PhoneApplicationPage
    {
        public Load()
        {
            InitializeComponent();
        }
        
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (this.NavigationContext.QueryString.ContainsKey("ProgramName") && this.NavigationContext.QueryString.ContainsKey("ProgramDate"))
            {               
                string param = this.NavigationContext.QueryString["ProgramName"];
                DateTime date = DateTime.Parse(this.NavigationContext.QueryString["ProgramDate"]);

                DataContext = ProcessInspector.Instance.getProgramByName(param,date);
            }
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);          
        }

        private string FetchBackgroundColor()
        {
            return IsBackgroundBlack() ? "#000;" : "#fff";
        }

        private string FetchFontColor()
        {
            return IsBackgroundBlack() ? "#fff;" : "#000";
        }

        private static bool IsBackgroundBlack()
        {
            return FetchBackGroundColor() == "#FF000000";
        } 

        private static string FetchBackGroundColor()
        {
            string color;
            Color mc =
              (Color)Application.Current.Resources["PhoneBackgroundColor"];
            color = mc.ToString();
            return color;
        }

        private void SetBackground()
        {
            Color mc =
              (Color)Application.Current.Resources["PhoneBackgroundColor"];
            browserDesc.Background = new SolidColorBrush(mc);
        }

        private void browserDesc_LayoutUpdated(object sender, EventArgs e)
        {
            string fontColor = FetchFontColor();
            string backgroundColor = FetchBackgroundColor();
            SetBackground();

            string desc = ((Program)DataContext).LongDescription;
            desc = convertExtendedAscii(desc);
            //usuwamy obrazek kategorii, który się nie ładuje
            desc = Regex.Replace(desc, @"(<p class=""articleDate"">kategoria:.*?</p>)", ""); 
            //usuwamy nagłówek z tytułem filmu (bo na chuj?)
            desc = Regex.Replace(desc, @"(<h2>.*?</h2>)", "");
            //usuwamy linki, zostawiając ich treść
            desc = Regex.Replace(desc, @"<a.*?>(?<text>.*?)</a>", "${text}");
            
            string html = String.Format("<!doctype html><html><head><meta name=\"viewport\" content=\"width=320,user-scalable=no\" /><style type=\"text/css\">body {{ color: {1}; background-color: {2}; font-size: medium; padding-bottom: 20px; }} a, a:visited {{ color: inherit }} #newsAddContent {{ float: left; padding: 0 5px 5px 0; }}</style></head><body>{0}</body></html>", desc, fontColor, backgroundColor);
            browserDesc.NavigateToString(html);
        }

        protected static string convertExtendedAscii(string HTML)
        {
            string retVal = "";
            char[] s = HTML.ToCharArray();

            foreach (char c in s)
            {
                if (Convert.ToInt32(c) > 127)
                    retVal += "&#" + Convert.ToInt32(c) + ";";
                else
                    retVal += c;
            }

            return retVal;
        }  

        private void btnRemind_Click(object sender, RoutedEventArgs e)
        {
            try
            {                
                int index = 0;
                PathConfig.DeserializeData();
                if (ProcessInspector.Instance.ProgramsReminder != null)
                {
                    index = ProcessInspector.Instance.ProgramsReminder.Count + 1;
                    ProcessInspector.Instance.ProgramsReminder.Add(new Entity.ProgramReminder()
                    {
                        ID = index,
                        Name = ProgramName.Text,
                        Station = ProcessInspector.Instance.ActualStation.StationName,
                        DateStart = DateTime.Parse(DateBegin.Text)
                    });
                }
                else
                {
                    ObservableCollection<ProgramReminder> tmp = new ObservableCollection<ProgramReminder>();
                    tmp.Add(new Entity.ProgramReminder()
                    {
                        ID = index,
                        Name = ProgramName.Text,
                        Station = ProcessInspector.Instance.ActualStation.StationName,
                        DateStart = DateTime.Parse(DateBegin.Text)
                    });
                    ProcessInspector.Instance.ProgramsReminder = tmp;
                }

                StringBuilder sb = new StringBuilder("Za chiwlę rozpocznie się program który chciałeś oglądnąć!\n Klinknij tutaj aby przejść do szczegółów...");
               
                Reminder programReminder = new Reminder(index.ToString());

                if (SettingsHelper.getValue("autoTime") == "1")
                {
                    int minutesBefore = int.Parse(SettingsHelper.getValue("timeToStart"));
                    DateTime startReminder = DateTime.Parse(DateBegin.Text).AddMinutes(-minutesBefore);
                    //DateTime tmpD = DateTime.Parse(DateBegin.Text);
                    //DateTime tmpD2 = DateTime.Now;
                    double minutesToAdd = startReminder.Subtract(DateTime.Now).TotalMinutes;
                    if (minutesToAdd > 0)
                        programReminder.BeginTime = DateTime.Now.AddMinutes(minutesToAdd);
                    else
                        throw (new Exception("Program musi rozpocząć się później niż aktualny czas."));
                }
                else
                {
                    double minutesToAdd = DateTime.Parse(DateBegin.Text).Subtract(DateTime.Now).TotalMinutes;
                    if (minutesToAdd > 0)
                        programReminder.BeginTime = DateTime.Now.AddMinutes(minutesToAdd);
                    else
                        throw (new Exception("Program musi rozpocząć się później niż aktualny czas."));
                        //.BeginTime = DateTime.Now.AddSeconds(10);
                }
                                
                // do testów 1 minuta, potem musi być powiązanie z ustawieniami i faktycznym godzinami...
                //programReminder.BeginTime = DateTime.Now.AddMinutes(1);               
                programReminder.Content = sb.ToString();
                programReminder.RecurrenceType = RecurrenceInterval.None;
                programReminder.NavigationUri = new Uri("/Pages/ReminderPage.xaml?ProgramName=" + programReminder.Name, UriKind.Relative);

                ScheduledActionService.Add(programReminder);

                PathConfig.SerializeData();

                MessageBox.Show("Poprawnie dodano przypomnienie.", "Operacja zakończona powodzeniem.", MessageBoxButton.OK);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem z dodaniem zdarzenia.\n\n" + ex.Message, "Błąd.", MessageBoxButton.OK);
            }
        }
    }
}