﻿#define debug2

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using DeepForest.Phone.Assets.Tools;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Windows.Navigation;
using System.Windows.Data;
using System.Threading;


namespace ProgramTV
{
    public partial class MainPage : PhoneApplicationPage
    {
        object _selectedItem;
        
        private bool IsSearchableState
        {
            get { return (tbSearch.Text.Length == 0); }
        }

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            Utility.SettingsHelper.setServerLink();
            
            if (Utility.SettingsHelper.getValue("download") == "1")
            {
                if (Utility.PathConfig.checkFolder(string.Format("{0:yyyy-MM-dd}", DateTime.Now)) == false)
                {
                    Utility.DownloaderHelper downloader = new Utility.DownloaderHelper();
                    downloader.GetFile(string.Format("{0:yyyy-MM-dd}", DateTime.Now));
                }
            }

            //if (Utility.PathConfig.getFolders() == null)
            //{
            //    NotificationTool.Show(
            //        "Brak programu...",
            //        "Czy chcesz przejść do zakładki pobierania programu?",
            //        new NotificationAction("Tak", () =>
            //        {
            //            NavigationService.Navigate(new Uri("/Pages/Download.xaml", UriKind.Relative));
            //        }),
            //        new NotificationAction("Nie", () => { }));
            //}

            this.Loaded += new RoutedEventHandler(MainPage_Loaded);
        }

        public bool ShowProgress
        {
            get { return (bool)GetValue(ShowProgressProperty); }
            set { SetValue(ShowProgressProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowProgress.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowProgressProperty =
            DependencyProperty.Register("ShowProgress", typeof(bool), typeof(MainPage), new PropertyMetadata(true));

        // Load code for the ViewModel Items
        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {            
                ThreadPool.QueueUserWorkItem(
                   (o) =>
                   {                                                            
                       try
                       {
                           if(ProcessInspector.Instance.ActualDate == null)
                                ProcessInspector.Instance.ActualDate = string.Format("{0:yyyy-MM-dd}", DateTime.Now);                           
              #if debug
                           ProcessInspector.Instance.ActualDate = "2011-11-28";
                #endif
                           ProcessInspector.Instance.Stations = XMLStationHelper.getStations(
                                ProcessInspector.Instance.ActualDate);                          
                       }
                       catch (Exception ex)
                       {
                           this.Dispatcher.BeginInvoke(() =>
                           {
                               MessageBox.Show(ex.Message, "Błąd.", MessageBoxButton.OK);
                           });
                       }
                       finally
                       {

                           this.Dispatcher.BeginInvoke(() =>
                           {
                               lbStations.ItemsSource = ProcessInspector.Instance.Stations; //XMLStationHelper.getStations("Programs/2011-11-28");   
                               ProgramDate.Text = ProcessInspector.Instance.ActualDate;    
                           });

                           this.Dispatcher.BeginInvoke(
                               () =>
                               {
                                   ShowProgress = false;
                               });                                                   
                       }                      
                   });
                            
            }
            catch (Exception ex)
            {
                MessageBox.Show("Wystąpił błąd podczas wczytywanie programu.\n\n" + ex.Message, "Błąd.", MessageBoxButton.OK);
            }

            if (Utility.SettingsHelper.getValue("autoDelete") == "1")
                new Thread(new ThreadStart(() =>
                {
                    Utility.DeleteHelper.DeletePrograms();
                })).Start();
        }


        private void SettingsButton_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/Configuration.xaml", UriKind.Relative));
        }

        private void DownloadButton_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/Download.xaml", UriKind.Relative));
        }

        private void LoadButton_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/Remove.xaml", UriKind.Relative));
        }

        private void HistoryButton_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/ReminderHistory.xaml", UriKind.Relative));
        }

        private void AboutButton_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pages/About.xaml", UriKind.Relative));
        }


        private void btnSearchCancel_Click(object sender, RoutedEventArgs e)
        {
            tbSearch.Text = String.Empty;
            SearchPanel.Visibility = System.Windows.Visibility.Collapsed;
            RefreshStations();       
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            if (SearchPanel.Visibility == System.Windows.Visibility.Collapsed)
            {
                SearchPanel.Visibility = System.Windows.Visibility.Visible;
                tbSearch.Focus();                
            }
            else
            {
                tbSearch.Text = String.Empty;
                SearchPanel.Visibility = System.Windows.Visibility.Collapsed;
                RefreshStations();
            }
        }
              
        private void RefreshStations()
        {
            if (!IsSearchableState) return;

            this.lbStations.ItemsSource = ProcessInspector.Instance.Stations;  //XMLStationHelper.getStations("Programs/2011-11-28");
        }

                   
        private void ListBoxOne_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                // Capture selected item data
                _selectedItem = (sender as ListBox).SelectedItem;

                if (_selectedItem != null)
                {
                    ProcessInspector.Instance.ActualStation = (Station)_selectedItem;

                    if (Utility.SettingsHelper.getValue("notAll") == "0")
                        ProcessInspector.Instance.Programs = Parser.XMLProgramHelper.getProgram(
                            ProcessInspector.Instance.ActualDate,
                            ProcessInspector.Instance.ActualStation.ID);
                    else
                        ProcessInspector.Instance.Programs = Parser.XMLProgramHelper.getProgram(
                            ProcessInspector.Instance.ActualDate,
                            ProcessInspector.Instance.ActualStation.ID,
                            true);

                    PivotProgramDescription.Text = "Stacja => " + ProcessInspector.Instance.ActualStation.StationName;

                    SecondListBox.ItemsSource = ProcessInspector.Instance.Programs;

                    ProcessInspector.Instance.setCategories();

                    ProgramsCategories.ItemsSource = ProcessInspector.Instance.ActualCategories;
                    ProgramsCategories.SelectedItem = "Cały program";
                                      
                    MainPivot.SelectedIndex = 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczasz przechodzenia pomiędzy zakładkami.\n\n" + ex.Message, "Błąd.", MessageBoxButton.OK);
            }
        }

        private void FilterData()
        {
            try
            {
                if (lbStations != null)
                {
                    ICollectionView dataView = default(ICollectionView);

                    CollectionViewSource source = new CollectionViewSource();

                    source.Source = ProcessInspector.Instance.Stations; //XMLStationHelper.getStations("Programs/2011-11-28");

                    dataView = source.View;

                    dataView.Filter = prod => ((Station)prod).StationName.ToLower()
                                                 .Contains(tbSearch.Text.ToLower());

                    lbStations.ItemsSource = dataView;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas filtrowania stacji.\n\n" + ex.Message, "Błąd.", MessageBoxButton.OK);
            }
        }

        private void FilterCategory(string item)
        {
            try
            {
                if (ProgramsCategories.SelectedItem != null)
                {                    
                    if (item == "Cały program")
                    {
                        SecondListBox.ItemsSource = ProcessInspector.Instance.Programs;
                    }
                    else
                    {
                        ICollectionView dataView = default(ICollectionView);

                        CollectionViewSource source = new CollectionViewSource();

                        source.Source = ProcessInspector.Instance.Programs; //XMLStationHelper.getStations("Programs/2011-11-28");

                        dataView = source.View;

                        dataView.Filter = prod => ((Program)prod).Category.ToLower().Equals(item.ToLower());

                        SecondListBox.ItemsSource = dataView;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas filtrowania stacji.\n\n" + ex.Message, "Błąd.", MessageBoxButton.OK);
            }
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            RefreshStations();
            FilterData();
        }

        private void SecondListBox_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                _selectedItem = (sender as ListBox).SelectedItem;

                if (_selectedItem != null)
                {
                    string dest = "/Pages/Load.xaml?ProgramName=" + ((Program)_selectedItem).Name
                        + "&ProgramDate=" + ((Program)_selectedItem).Hour;
                    NavigationService.Navigate(new Uri(dest, UriKind.Relative));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczasz przechodzenia pomiędzy zakładkami.\n\n" + ex.Message, "Błąd.", MessageBoxButton.OK);
            }
        }
             
        private void ProgramsCategories_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ProgramsCategories.SelectedIndex != -1)
                FilterCategory(ProgramsCategories.SelectedItem.ToString());
        }

        private void MainPivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (SearchButton != null)
            {
                if (MainPivot.SelectedIndex == 0)
                    SearchButton.IsEnabled = true;
                else if (MainPivot.SelectedIndex == 1)
                    SearchButton.IsEnabled = false;
            }
        }         
    }    
}