﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Resources;
using System.IO;
using System.IO.IsolatedStorage;
using System.Collections.Generic;
using System.Text;

namespace ProgramTV
{
    public class ZipHelper
    {
        internal void UnzipFile(string filePath)
        {
            StreamResourceInfo fileInfo = App.GetResourceStream(new Uri(filePath, UriKind.Relative));
            UnzipFile(ref fileInfo);
        }

        internal void UnzipFile(ref StreamResourceInfo fileInfo)
        {
            string[] fileNames = GetFileNamesFromZip(fileInfo.Stream);
            foreach (string fileName in fileNames)
            {
                StreamResourceInfo audioFile = App.GetResourceStream(fileInfo, new Uri(fileName, UriKind.Relative));
                if (audioFile != null)
                    SaveFile(audioFile.Stream, fileName);
            }
        }

        private void SaveFile(Stream stream, string relativefilePath)
        {
            //int lastSlashPos = relativefilePath.LastIndexOf('/');
            //int substringLen = relativefilePath.Length - relativefilePath.LastIndexOf('/');
            //string fileName = relativefilePath.Substring(lastSlashPos + 1, substringLen - 1);
            using (var appStore = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (appStore.FileExists(relativefilePath))
                    appStore.DeleteFile(relativefilePath);

                CreateDirectory(appStore, relativefilePath);
                using (IsolatedStorageFileStream isoStream = appStore.CreateFile(relativefilePath))
                {
                    byte[] buffer = new byte[stream.Length];
                    using (BinaryWriter fileWriter = new BinaryWriter(isoStream))
                    {
                        stream.Read(buffer, 0, (int)stream.Length);
                        fileWriter.Write(buffer);
                    }
                }
            }
        }

        private void CreateDirectory(IsolatedStorageFile appStore, string relativefilePath)
        {
            if (relativefilePath.Contains("/"))
            {
                int lastSlashPos = relativefilePath.LastIndexOf('/');
                int substringLen = relativefilePath.Length - relativefilePath.LastIndexOf('/');

                string directoryName = relativefilePath.Remove(lastSlashPos, substringLen);
                if (!string.IsNullOrEmpty(directoryName))
                    appStore.CreateDirectory(directoryName);
            }
        }

        /// <summary>
        /// Reads the file names from the header of the zip file
        /// </summary>
        /// <param name="zipStream">The stream to the zip file</param>
        /// <returns>An array of file names stored within the zip file. These file names may also include relative paths.</returns>
        private string[] GetFileNamesFromZip(System.IO.Stream zipStream)
        {
            List<string> names = new List<string>();
            BinaryReader reader = new BinaryReader(zipStream);
            while (reader.ReadUInt32() == 0x04034b50)
            {
                // Skip the portions of the header we don't care about
                reader.BaseStream.Seek(14, SeekOrigin.Current);
                uint compressedSize = reader.ReadUInt32();
                uint uncompressedSize = reader.ReadUInt32();
                int nameLength = reader.ReadUInt16();
                int extraLength = reader.ReadUInt16();
                byte[] nameBytes = reader.ReadBytes(nameLength);
                names.Add(Encoding.UTF8.GetString(nameBytes, 0, nameLength));
                reader.BaseStream.Seek(extraLength + compressedSize, SeekOrigin.Current);

            }
            // Move the stream back to the begining
            zipStream.Seek(0, SeekOrigin.Begin);
            return names.ToArray();

        }
    }
}
