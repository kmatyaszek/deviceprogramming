<?php
class XMLSerializer {
    // functions adopted from http://www.sean-barton.co.uk/2009/03/turning-an-array-or-object-into-xml-using-php/
    public static function generateValidXmlFromObj(stdClass $obj, $node_block='nodes', $node_name='node', $notEscapedKeys = array()) {
        $arr = get_object_vars($obj);
        return self::generateValidXmlFromArray($arr, $node_block, $node_name, $notEscapedKeys);
    }

    public static function generateValidXmlFromArray($array, $node_block='nodes', $node_name='node',$notEscapedKeys = array()) {
        $xml = '<?xml version="1.0" encoding="UTF-8" ?>';

        $xml .= '<' . $node_block . '>';
        $xml .= self::generateXmlFromArray($array, $node_name, $notEscapedKeys);
        $xml .= '</' . $node_block . '>';

        return $xml;
    }

    private static function generateXmlFromArray($array, $node_name, $notEscapedKeys, $esc = true) {
        $xml = '';

        if (is_array($array) || is_object($array)) {
            foreach ($array as $key=>$value) {
                if (is_numeric($key)) {
                    $key = $node_name;
                }

                $escapeNext = true;
                if (in_array($key, $notEscapedKeys))
                    $escapeNext = false;
                    
                $xml .= '<' . $key . '>' . self::generateXmlFromArray($value, $node_name, $notEscapedKeys, $escapeNext) . '</' . $key . '>';
            }
        } else {
            if ($esc)
                $xml = htmlspecialchars($array, ENT_QUOTES);
            else
                if (empty($array))
                    $xml = '';
                else
                    $xml = '<![CDATA[' . $array . ']]>';
        }

        return $xml;
    }
}
