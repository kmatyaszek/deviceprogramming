﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using ProgramTV.ViewModels;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.IO;

namespace ProgramTV
{
    public class XMLStationHelper
    {
        public static ObservableCollection<Station> getStations(string folderName)
        {
            ObservableCollection<Station> stations = new ObservableCollection<Station>();

            string tempPath = System.IO.Path.Combine(folderName, "stations.xml");

            using (var store = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (store.FileExists(System.IO.Path.Combine("Programs/" + folderName, "stations.xml")) == false)
                {
                    stations.Add(new Station()
                    {
                        StationName = "Nie ma programu na dzisiaj.",
                        StationDetails = "Proszę ściągnąć program na dzisiaj...",
                        ID = 0,
                        FileName = "/Logo/brak.png"
                    });

                    return stations;
                }
                else
                {
                    //using (var readStream = new IsolatedStorageFileStream(System.IO.Path.Combine(folderName, "stations.xml"), FileMode.Open, FileAccess.ReadWrite, store))
                    using (IsolatedStorageFileStream readStream = store.OpenFile(System.IO.Path.Combine("Programs/" + folderName, "stations.xml"), FileMode.Open))
                    {
                        
                        List<Station> tmp = new List<Station>();

                        XmlReader reader = XmlReader.Create(readStream);

                        XDocument xDoc = XDocument.Load(reader);

                        tmp = (from s in xDoc.Descendants("station")
                               select new Station()
                               {
                                   ID = int.Parse(s.Element("id").Value),
                                   StationName = s.Element("name").Value,
                                   StationDetails = "Szczegółowe dane...",
                                   FileName = "/Logo/" + Utility.ImageHelper.getImageName(s.Element("name").Value) //"/Logo/brak.png"
                               }).ToList();

                        return new ObservableCollection<Station>(tmp);
                    }
                }
            }
        }

        public static ObservableCollection<StationViewModel> getStationsViewModel(string folderName)
        {
            List<StationViewModel> stations = new List<StationViewModel>();

            string tempPath = System.IO.Path.Combine(folderName, "stations.xml");

            using (var store = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (store.FileExists(System.IO.Path.Combine(folderName, "stations.xml")) == false)
                {                    
                    ObservableCollection<StationViewModel> tmp = new ObservableCollection<StationViewModel>();
                    tmp.Add(new StationViewModel()
                    {
                        StationName = "Nie ma programu na dzisiaj.",
                        StationDetails = "Proszę ściągnąć program na dzisiaj..."
                    });

                    return tmp;
                }
                else
                {
                    //using (var readStream = new IsolatedStorageFileStream(System.IO.Path.Combine(folderName, "stations.xml"), FileMode.Open, FileAccess.ReadWrite, store))
                    using (IsolatedStorageFileStream readStream = store.OpenFile(System.IO.Path.Combine(folderName, "stations.xml"), FileMode.Open))
                    {
                        XmlReader reader = XmlReader.Create(readStream);

                        XDocument xDoc = XDocument.Load(reader);

                        stations = (from s in xDoc.Descendants("station")
                                    select new StationViewModel()
                                    {
                                        StationName = s.Element("name").Value,
                                        StationDetails = "Szczegółowe dane..."
                                    }).ToList();

                        return new ObservableCollection<StationViewModel>(stations);
                    }
                }
            }
        }
      
    }
}
