﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Microsoft.Phone.Controls;

namespace ProgramTV
{
    public class AutosuggestTextBox : AutoCompleteBox, INotifyPropertyChanged
    {
        private bool criteriaChanged;
        private bool isEmpty;
        private bool isInternalTextChanged;
        private string oldText;

        public AutosuggestTextBox()
        {
            IsEmpty = true;
            PerformLostFocus();
            TextChanged += (sender, e) => OnTextChanged();
            // Add code to hook the selection changed - then when they select an item in the dropdown, we can close the dropdown
            this.SelectionChanged += (sender, e) =>
                {
                    if (this.SelectedItem != null)
                    {
                        this.IsDropDownOpen = false;
                    }
                };
        }

        public bool IsEmpty
        {
            get { return isEmpty; }
            private set
            {
                isEmpty = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("IsEmpty"));
                InvokePropertyChanged(new PropertyChangedEventArgs("IsSearch"));
            }
        }

        public bool IsSearch
        {
            get { return !IsEmpty; }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        public void Clear()
        {
            IsEmpty = true;
            PerformLostFocus();
            InvokeSearchStringChanged(EventArgs.Empty);
        }

        protected override void OnGotFocus(RoutedEventArgs e)
        {
            base.OnGotFocus(e);
            PerformGotFocus();
        }

        protected override void OnLostFocus(RoutedEventArgs e)
        {
            base.OnLostFocus(e);
            PerformLostFocus();
        }

        private void InvokePropertyChanged(PropertyChangedEventArgs e)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, e);
        }

        private void InvokeSearchStringChanged(EventArgs e)
        {
            criteriaChanged = false;
            var searchStringChangedHandler = SearchStringChanged;
            if (searchStringChangedHandler != null) searchStringChangedHandler(this, e);
        }

        private void OnTextChanged()
        {
            if (oldText == Text) return;
            oldText = Text;
            if (isInternalTextChanged)
            {
                isInternalTextChanged = false;
                return;
            }
            IsEmpty = Text.Length == 0;
            criteriaChanged = true;
            InvokeSearchStringChanged(EventArgs.Empty);
        }

        private void PerformGotFocus()
        {
            if (!IsEmpty) return;
            isInternalTextChanged = true;
            Text = "";
            FontStyle = FontStyles.Normal;
            FontWeight = FontWeights.Normal;
            Foreground = new SolidColorBrush(Colors.Black);
            Opacity = 1;
        }

        private void PerformLostFocus()
        {
            if (!IsEmpty) return;
            isInternalTextChanged = true;
            Text = "Szukaj";
            FontStyle = FontStyles.Italic;
            FontWeight = FontWeights.Thin;
            Foreground = new SolidColorBrush(Colors.LightGray);
            if (!criteriaChanged) return;
            InvokeSearchStringChanged(EventArgs.Empty);
        }

        public event EventHandler SearchStringChanged;
    }
}