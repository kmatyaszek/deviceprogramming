﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using ProgramTV.ViewModels;


namespace ProgramTV
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public  MainViewModel(ObservableCollection<StationViewModel> items = null)
        {
            if (items == null)
                Items = new ObservableCollection<StationViewModel>();
            else
                Items = items;
        }

        /// <summary>
        /// A collection for ItemViewModel objects.
        /// </summary>
        public ObservableCollection<StationViewModel> Items { get; set; }

        //private string _sampleProperty = "Dane do programu telewizyjnego";
        ///// <summary>
        ///// Sample ViewModel property; this property is used in the view to display its value using a Binding
        ///// </summary>
        ///// <returns></returns>
        //public string SampleProperty
        //{
        //    get
        //    {
        //        return _sampleProperty;
        //    }
        //    set
        //    {
        //        if (value != _sampleProperty)
        //        {
        //            _sampleProperty = value;
        //            NotifyPropertyChanged("SampleProperty");
        //        }
        //    }
        //}

        public bool IsDataLoaded
        {
            get;
            private set;
        }

        /// <summary>
        /// Creates and adds a few ItemViewModel objects into the Items collection.
        /// </summary>
        public void LoadData()
        {
            if (Items.Count != 0)
                Items.Clear();

            //Items.Add(new StationViewModel() { StationName = "TVP1", StationDetails = "Teraz leci ble ble ble" });
            //Items.Add(new StationViewModel() { StationName = "TVP2", StationDetails = "Teraz leci ble ble ble" });
            //Items.Add(new StationViewModel() { StationName = "TVP3", StationDetails = "Teraz leci ble ble ble" });
            //Items.Add(new StationViewModel() { StationName = "Polsat", StationDetails = "Teraz leci ble ble ble" });
            //Items.Add(new StationViewModel() { StationName = "TVN", StationDetails = "Teraz leci ble ble ble" });

            try
            {
                Items = XMLStationHelper.getStationsViewModel(System.IO.Path.GetFileNameWithoutExtension(string.Format("{0:yyyy-MM-dd}", DateTime.Now)));
            }
            catch (Exception ex)
            { }

            IsDataLoaded = true;
        }

        public void SetItems(ObservableCollection<StationViewModel> items)
        {
            Items = items;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}