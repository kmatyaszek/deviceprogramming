﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Globalization;

namespace ProgramTV.Entity
{
    public class ProgramReminder : INotifyPropertyChanged
    {
        private int id;
        private string name;
        private string station;
        private DateTime dateStart;

        public int ID
        {
            get { return id; }
            set
            {
                id = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ID"));
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Name"));
            }
        }

        public string Station
        {
            get { return station; }
            set
            {
                station = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Station"));
            }
        }

        public DateTime DateStart
        {
            get { return dateStart; }
            set
            {
                dateStart = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("DateStart"));
            }
        }


        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        public bool IsValid
        {
            get
            {
                if (this.name == null || this.name.Length == 0) return false;
                return true;
            }
        }

        public void Deallocate()
        {
            ProcessInspector.Instance.ProgramsReminder.Remove(this);            
        }

        public override string ToString()
        {
            return string.Format(CultureInfo.CurrentCulture, "{0}", Name);
        }

        private void InvokePropertyChanged(PropertyChangedEventArgs e)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, e);
        }
    }
}
