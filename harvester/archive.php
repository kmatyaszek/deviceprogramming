<?php

$date = $_GET['date'];

$archFile = "data/$date.zip";
$filePath = "data/$date";

if (is_file($archFile))
    header("Location: $archFile");
else
{
    if (is_dir($filePath))
    {
        $zip = new ZipArchive();
        if ($zip->open($archFile, ZIPARCHIVE::CREATE)!==TRUE) 
        {
            exit("cannot open <$archFile>\n");
        }
        
        $zip->addFile("data/stations.xml", "stations.xml");
        
        $dir = opendir($filePath);
        while ($dir && ($file = readdir($dir)) !== false) 
        {
            if ($file != '.' && $file != '..')
            {
                $zip->addFile($filePath . '/' . $file, $file);
            }
        }
        $zip->setArchiveComment('Pobrany program telewizyjny z dnia ' . $date);
        $zip->close();
        
        header("Location: $archFile");
    }
    else
    {
        header("HTTP/1.0 404 Not Found");
        header("Content-Type: text/plain; charset=UTF-8");
        echo "Nie można wygenerować paczki, brak pobranych danych.";
    }
}