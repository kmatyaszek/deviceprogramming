﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ProgramTV.Utility
{
    public static class ImageHelper
    {
        #region list logo
        static List<string> listLogo = new List<string>();

        static ImageHelper()
        {
            listLogo.Add("13eme rue.png");
            listLogo.Add("13th street.png");
            listLogo.Add("3sat.png");
            listLogo.Add("4fun.tv.png");
            listLogo.Add("ale kino.png");
            listLogo.Add("alpha tv.png");
            listLogo.Add("animal planet hd.png");
            listLogo.Add("animal planet.png");
            listLogo.Add("ard.png");
            listLogo.Add("arte.png");
            listLogo.Add("atv.png");
            listLogo.Add("axn +1.png");
            listLogo.Add("axn action.png");
            listLogo.Add("axn crime.png");
            listLogo.Add("axn hd.png");
            listLogo.Add("axn sci-fi.png");
            listLogo.Add("axn scifi.png");
            listLogo.Add("axn.png");
            listLogo.Add("bayern 3.png");
            listLogo.Add("bbc cbeebies poland.png");
            listLogo.Add("bbc entertainment.png");
            listLogo.Add("bbc knowledge poland.png");
            listLogo.Add("bbc lifestyle poland.png");
            listLogo.Add("bbc world.png");
            listLogo.Add("beate-uhse.tv.png");
            listLogo.Add("berlin 1.png");
            listLogo.Add("bloomberg.png");
            listLogo.Add("blue movie 1.png");
            listLogo.Add("blue movie 2.png");
            listLogo.Add("blue movie 3.png");
            listLogo.Add("boomerang.png");
            listLogo.Add("brak.png");
            listLogo.Add("canal+ 3d.png");
            listLogo.Add("canal+ film hd.png");
            listLogo.Add("canal+ film.png");
            listLogo.Add("canal+ gol.png");
            listLogo.Add("canal+ hd.png");
            listLogo.Add("canal+ hits sportweekend.png");
            listLogo.Add("canal+ sport 1.png");
            listLogo.Add("canal+ sport 2 hd.png");
            listLogo.Add("canal+ sport 2.png");
            listLogo.Add("canal+ sport 3.png");
            listLogo.Add("canal+ sport extra.png");
            listLogo.Add("canal+ sport hd.png");
            listLogo.Add("canal+ sport.png");
            listLogo.Add("canal+.png");
            listLogo.Add("cartoon network.png");
            listLogo.Add("cinemax.png");
            listLogo.Add("cinemax2.png");
            listLogo.Add("classica.png");
            listLogo.Add("cnbc europe.png");
            listLogo.Add("cnn.png");
            listLogo.Add("com tv.png");
            listLogo.Add("comedyctrl+1.png");
            listLogo.Add("ct1.png");
            listLogo.Add("ct2.png");
            listLogo.Add("davinci.png");
            listLogo.Add("discovery hd.png");
            listLogo.Add("discovery science.png");
            listLogo.Add("discovery world.png");
            listLogo.Add("discovery.png");
            listLogo.Add("disney channel.png");
            listLogo.Add("disney junior.png");
            listLogo.Add("disney xd +1.png");
            listLogo.Add("disney xd hd.png");
            listLogo.Add("disney xd.png");
            listLogo.Add("domo.png");
            listLogo.Add("dsf.png");
            listLogo.Add("dw-tv europa.png");
            listLogo.Add("edusat.png");
            listLogo.Add("espn.png");
            listLogo.Add("euronews.png");
            listLogo.Add("eurosport 2 hd.png");
            listLogo.Add("eurosport hd.png");
            listLogo.Add("eurosport.png");
            listLogo.Add("eurosport2.png");
            listLogo.Add("extreme sports.png");
            listLogo.Add("fashion tv.png");
            listLogo.Add("filmbox extra.png");
            listLogo.Add("filmbox family.png");
            listLogo.Add("filmbox hd.png");
            listLogo.Add("filmbox.png");
            listLogo.Add("fox hd.png");
            listLogo.Add("foxlife.png");
            listLogo.Add("fox.png");
            listLogo.Add("france 2.png");
            listLogo.Add("france 3.png");
            listLogo.Add("hamburg1.png");
            listLogo.Add("hbo comedy.png");
            listLogo.Add("hbo hd.png");
            listLogo.Add("hbo.png");
            listLogo.Add("hbo2.png");
            listLogo.Add("heimatkanal.png");
            listLogo.Add("hot.png");
            listLogo.Add("hyper+.png");
            listLogo.Add("italia1.png");
            listLogo.Add("itvn.png");
            listLogo.Add("kabel eins.png");
            listLogo.Add("kidsco.png");
            listLogo.Add("kika(kinderkanal).png");
            listLogo.Add("kino polska.png");
            listLogo.Add("kuchnia.png");
            listLogo.Add("la7.png");
            listLogo.Add("m6.png");
            listLogo.Add("mdr info.png");
            listLogo.Add("minimini.png");
            listLogo.Add("movies24.png");
            listLogo.Add("mtv base.png");
            listLogo.Add("mtv germany niem.png");
            listLogo.Add("mtv.png");
            listLogo.Add("mtv2.png");
            listLogo.Add("ntv.png");
            listLogo.Add("n24.png");
            listLogo.Add("nat geo wild.png");
            listLogo.Add("national geographic channel.png");
            listLogo.Add("national geographic hd.png");
            listLogo.Add("nickelodeon.png");
            listLogo.Add("nova.png");
            listLogo.Add("nsport.png");
            listLogo.Add("orangesport.png");
            listLogo.Add("orf1.png");
            listLogo.Add("orf2.png");
            listLogo.Add("phoenix.png");
            listLogo.Add("planete+.png");
            listLogo.Add("polonia1.png");
            listLogo.Add("polsat 2.png");
            listLogo.Add("polsat cafe.png");
            listLogo.Add("polsat play.png");
            listLogo.Add("polsat sport extra.png");
            listLogo.Add("polsat sport hd.png");
            listLogo.Add("polsat sport news.png");
            listLogo.Add("polsat sport.png");
            listLogo.Add("polsat.png");
            listLogo.Add("pro7.png");
            listLogo.Add("rai1.png");
            listLogo.Add("rai2.png");
            listLogo.Add("rai3.png");
            listLogo.Add("religia.tv.png");
            listLogo.Add("rete 4.png");
            listLogo.Add("rtl.png");
            listLogo.Add("rtl2.png");
            listLogo.Add("sat 1.png");
            listLogo.Add("sf 1.png");
            listLogo.Add("sf 2.png");
            listLogo.Add("sportklub+ poland.png");
            listLogo.Add("sportklub.png");
            listLogo.Add("startv.png");
            listLogo.Add("sudwest3.png");
            listLogo.Add("super rtl.png");
            listLogo.Add("superstacja.png");
            listLogo.Add("t24.png");
            listLogo.Add("tc cinema.png");
            listLogo.Add("tc star.png");
            listLogo.Add("tcm.png");
            listLogo.Add("tele5.png");
            listLogo.Add("teletoon +1.png");
            listLogo.Add("tf1.png");
            listLogo.Add("theaterkanal.png");       
            listLogo.Add("ti2.png");
            listLogo.Add("tlc.png");
            listLogo.Add("toya.png");
            listLogo.Add("travel channel.png");
            listLogo.Add("tsi.png");
            listLogo.Add("tsr1.png");
            listLogo.Add("tsr2.png");
            listLogo.Add("tv 1000 hd.png");
            listLogo.Add("tv biznes.png");
            listLogo.Add("tv puls.png");
            listLogo.Add("tv.berlin.png");
            listLogo.Add("tv4.png");
            listLogo.Add("tv5monde europe.png");
            listLogo.Add("tve.png");
            listLogo.Add("tvn 24.png");
            listLogo.Add("tvn hd.png");
            listLogo.Add("tvn meteo.png");
            listLogo.Add("tvn 7 siedem.png");
            listLogo.Add("tvn style.png");
            listLogo.Add("tvn turbo.png");
            listLogo.Add("tvn.png");
            listLogo.Add("tvp bialystok.png");
            listLogo.Add("tvp bydgoszcz.png");
            listLogo.Add("tvp gdansk.png");
            listLogo.Add("tvp hd.png");
            listLogo.Add("tvp historia.png");
            listLogo.Add("tvp info.png");
            listLogo.Add("tvp katowice.png");
            listLogo.Add("tvp krakow.png");
            listLogo.Add("tvp kultura.png");
            listLogo.Add("tvp lodz.png");
            listLogo.Add("tvp lublin.png");
            listLogo.Add("tvp olsztyn.png");
            listLogo.Add("tvp polonia.png");
            listLogo.Add("tvp poznan.png");
            listLogo.Add("tvp regionalna.png");
            listLogo.Add("tvp rzeszow.png");
            listLogo.Add("tvp seriale.png");
            listLogo.Add("tvp sport.png");
            listLogo.Add("tvp szczecin.png");
            listLogo.Add("tvp warszawa.png");
            listLogo.Add("tvp1.png");
            listLogo.Add("tvp2.png");
            listLogo.Add("tvs.png");
            listLogo.Add("universal channel.png");
            listLogo.Add("vh1 classic.png");
            listLogo.Add("vh1.png");
            listLogo.Add("viasat explorer.png");
            listLogo.Add("viasat history.png");
            listLogo.Add("viva polska.png");
            listLogo.Add("viva1.png");
            listLogo.Add("vox.png");
            listLogo.Add("wdr.png");
            listLogo.Add("wedding tv.png");
            listLogo.Add("wroclaw.png");
            listLogo.Add("wtk poznan.png");
            listLogo.Add("zdf.png");
            listLogo.Add("zone club.png");
            listLogo.Add("zone europa.png");
            listLogo.Add("zone reality.png");
            listLogo.Add("zone romantica.png");
        }

#endregion

        public static string getImageName(string str)
        {
            //System.Diagnostics.Debug.WriteLine(str);

            try
            {
                if (str == "ATV (niem.)")
                    return "atv.png";

                string[] subs;

                str = str.ToLower();

                //ucinamy ogony
                char[] polskie = new char[] { 'ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ż', 'ź' };
                char[] normalne = new char[] { 'a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z' };

                for (int i = 0; i < str.Length; i++)
                    for (int j = 0; j < 9; j++)
                        if (str[i] == polskie[j])
                        {
                            str = str.Remove(i,1);
                            str = str.Insert(i, normalne[j].ToString());
                        }

                //zmieniamy zle znaki w spacje         np rai-2  -->  rai 2
                for (int i = 0; i < str.Length; i++)
                    if (!((str[i] >= '0' && str[i] <= '9') || (str[i] >= 'a' && str[i] <= 'z')) && str[i] != ' ')
                        str = str.Replace(str[i], ' ');

                //usuwamy podwójne spacje
                //for (int i = 1; i < str.Length; i++)
                //    if (str[i] == ' ' && str[i - 1] == ' ')
                //    {
                //        //str.erase(i - 1, 1); i--;
                //        str.Remove(i - 1, 1);
                //        i--;
                //    }

              

                //wstawiamy spacje miedzy litery i liczby
                for (int i = 1; i < str.Length; i++)
                    if ((str[i] >= 'a' && str[i] <= 'z' && str[i - 1] >= '0' && str[i - 1] <= '9') || (str[i - 1] >= 'a' && str[i - 1] <= 'z' && str[i] >= '0' && str[i] <= '9'))
                    {
                        //str.insert(i," ",1);
                        str = str.Insert(i, " ");
                    }

                str = Regex.Replace(str, @"\s+", " ");

                //dodajemy slowa do listy
                subs = str.Split(new char[] { ' ' });

                int ii = subs.Length;
                int ile = 0;
                string name;
                int curr_match = 0;
                int best_match_i = -1000;
                float avg = 0;    
                                             
                foreach (var item in listLogo)              
                {                          
                      ++ile;    
                      curr_match=0;
                      int max=0;
                      name = item;
                      
                      for (int j = 0; j < ii; j++)
                      {
                         
                          if (name.IndexOf(subs[j], 0) != -1)
                          {
                              ++max;
                              curr_match += subs[j].Length;
                          }
                      }
                   
                      curr_match -= Math.Abs((int)((name.Length-3-ii) - curr_match));
                      if (max == ii) curr_match += 8;
                      avg += curr_match;
                      if (curr_match > best_match_i)
                      {
                          str = name; best_match_i = curr_match;
                      }    
                    
                }

                avg = avg / (float)ile;
                if (avg + 8 > best_match_i)
                {
                    str = "brak.png";
                }
             
                return str;
            }
            catch (Exception ex)
            {
                throw (new Exception("Problem ze znalezieniem pliku...",ex));
            }          
        }
    }
}
