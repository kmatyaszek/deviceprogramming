﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProgramTV
{
    public class Program
    {
        public string Name { get; set; }
        public System.DateTime Hour { get; set; }
        /// <remarks>Szczegółowy typ, np. serial animowany</remarks>
        public string Type { get; set; }
        /// <remarks>Ogólna klasyfikacja programu wg Interia.pl</remarks>
        public string Category { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
    }
}
