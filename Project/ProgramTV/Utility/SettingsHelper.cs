﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using System.IO.IsolatedStorage;

namespace ProgramTV.Utility
{
    public class SettingsHelper
    {
        static IsolatedStorageSettings mySettings = IsolatedStorageSettings.ApplicationSettings;

        public static void setServerLink()
        {
            try
            {                
                setValue("server", "http://tv.amv.pl/archive.php?date=");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Wystąpił błąd podczas zapisu ustawień.\n\n" + ex.Message, "Błąd.", MessageBoxButton.OK);
            }
        }

        /// <summary>
        /// Ustawia wartość zmiennej.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void setValue(string key, string value)
        {
            try
            {
                if (mySettings.Contains(key))
                {
                    mySettings[key] = value;
                }
                else
                {
                    mySettings.Add(key, value);
                }
                mySettings.Save();
            }
            catch (Exception ex)
            {
                throw new Exception("Problem z zapisem ustawień.",ex);
            }
        }

        /// <summary>
        /// Pobiera wartość zmiennej o podanym kluczu.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string getValue(string key)
        {
            try
            {
                if (mySettings.Contains(key))
                {
                    return mySettings[key].ToString();
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Nie można pobrać wartości zmiennej.", ex);
            }
        }

        /// <summary>
        /// Usuwa klucz wraz z wartośćią przypisaną do niego.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool remove(string key)
        {
            try
            {
                mySettings.Remove(key);
                return true;
            }
            catch
            {
                return false;
            }            
        }
    
    }
}
