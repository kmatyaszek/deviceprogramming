﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace ProgramTV.ViewModels
{
    public class StationViewModel
    {
        private string _stationName;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string StationName
        {
            get
            {
                return _stationName;
            }
            set
            {
                if (value != _stationName)
                {
                    _stationName = value;
                    NotifyPropertyChanged("StationName");
                }
            }
        }

        private string _stationDetails;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string StationDetails
        {
            get
            {
                return _stationDetails;
            }
            set
            {
                if (value != _stationDetails)
                {
                    _stationDetails = value;
                    NotifyPropertyChanged("StationDetails");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

   
    /*
    public class StationViewModel : INotifyPropertyChanged
    {
        private string _stationName;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string StationName
        {
            get
            {
                return _stationName;
            }
            set
            {
                if (value != _stationName)
                {
                    _stationName = value;
                    NotifyPropertyChanged("StationName");
                }
            }
        }

        private string _stationDetails;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string StationDetails
        {
            get
            {
                return _stationDetails;
            }
            set
            {
                if (value != _stationDetails)
                {
                    _stationDetails = value;
                    NotifyPropertyChanged("StationDetails");
                }
            }
        }
              
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
     */


}
