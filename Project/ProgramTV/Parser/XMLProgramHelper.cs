﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using ProgramTV.ViewModels;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.IO;

namespace ProgramTV.Parser
{
    public class XMLProgramHelper
    {
        public static ObservableCollection<Program> getProgram(string date, int ID, bool notAll = false)
        {
            ObservableCollection<Program> programs = new ObservableCollection<Program>();

            string tempPath = System.IO.Path.Combine("Programs/" + date + "/", ID + ".xml");

            using (var store = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (store.FileExists(System.IO.Path.Combine("Programs/" + date + "/", ID + ".xml")) == false)
                {
                    programs.Add(new Program()
                    {
                        Name = "Brak programu...",
                        Category = "Brak",
                        Type = "Brak",
                        ShortDescription = "Brak",
                        LongDescription = "Brak",
                        Hour = DateTime.Now
                    });

                    return programs;
                }
                else
                {                    
                    using (IsolatedStorageFileStream readStream = store.OpenFile(System.IO.Path.Combine("Programs/" + date + "/", ID + ".xml"), FileMode.Open))
                    {

                        List<Program> tmp = new List<Program>();

                        XmlReader reader = XmlReader.Create(readStream);

                        XDocument xDoc = XDocument.Load(reader);

                        tmp = (from s in xDoc.Descendants("program")
                               select new Program()
                               {                                   
                                   Name = s.Element("name").Value,
                                   Hour = DateTime.Parse(s.Element("hour").Value),
                                   Type = s.Element("type").Value,
                                   ShortDescription = s.Element("short").Value,
                                   Category = s.Element("category").Value,
                                   LongDescription = s.Element("full").Value
                               }).ToList();

                        if (notAll == true)
                        {                           
                            int index = tmp.IndexOf(tmp.Select(x => x).Where(x => (x.Hour > DateTime.Now)).FirstOrDefault());

                            if (index >= 1)
                            {
                                var itemA = tmp[index];
                                
                                if (DateTime.Now < itemA.Hour)
                                {
                                    var itemB = tmp[index - 1];
                                    return new ObservableCollection<Program>(tmp.Where(x=> x.Hour >= itemB.Hour));
                                }
                            }                            
                            return new ObservableCollection<Program>(tmp.Select(x => x).Where(x => (x.Hour > DateTime.Now)));
                        }

                        return new ObservableCollection<Program>(tmp);
                    }
                }
            }
        }
    }
}
