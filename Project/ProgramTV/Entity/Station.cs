﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProgramTV
{
    public class Station
    {
        //private Program[] Programs;
        //private string Name;
        public int ID { get; set; }
        public string StationName { get; set; }
        public string StationDetails { get; set; }
        public string FileName { get; set; }
    }
}
