﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

using ProgramTV.Utility;

namespace ProgramTV.Pages
{
    public partial class Configuration : PhoneApplicationPage
    {
        public Configuration()
        {
            InitializeComponent();

            try
            {
                chbDownloadProgram.IsChecked =
                    SettingsHelper.getValue("download") != string.Empty ? SettingsHelper.getValue("download") == "1" ? true : false : false;

                chbAutoTime.IsChecked =
                    SettingsHelper.getValue("autoTime") != string.Empty ? SettingsHelper.getValue("autoTime") == "1" ? true : false : false;

                if (chbAutoTime.IsChecked == true)
                {
                    timeSlider.Value = int.Parse(SettingsHelper.getValue("timeToStart"));
                    timeTextBlock.Text = SettingsHelper.getValue("timeToStart") + " min."; 
                }
                

                chbDeleteAuto.IsChecked =
                    SettingsHelper.getValue("autoDelete") != string.Empty ? SettingsHelper.getValue("autoDelete") == "1" ? true : false : false;

                if (chbDeleteAuto.IsChecked == true)
                {
                    deleteSlider.Value = int.Parse(SettingsHelper.getValue("numOfDays"));
                    int days = int.Parse(SettingsHelper.getValue("numOfDays"));
                    if (days == 1)
                        deleteTextBlock.Text = days.ToString() + " dzień";
                    else
                        deleteTextBlock.Text = days.ToString() + " dni";                    
                }

                chbShow.IsChecked =
                   SettingsHelper.getValue("notAll") != string.Empty ? SettingsHelper.getValue("notAll") == "1" ? true : false : false;

                //tbServer.Text = SettingsHelper.getValue("server");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem z załadowaniem ustawień.\n\n" + ex.Message, "Błąd.", MessageBoxButton.OK);
            }
        }

        private void chbDownloadProgram_Checked(object sender, RoutedEventArgs e)
        {
            SettingsHelper.setValue("download", "1");
        }

        private void chbDownloadProgram_Unchecked(object sender, RoutedEventArgs e)
        {
            SettingsHelper.setValue("download", "0");
        }

        private void timeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (timeSlider != null)
            {
                int beforeTime = (int)(timeSlider.Value);
                timeTextBlock.Text = beforeTime.ToString() + " min.";
                SettingsHelper.setValue("timeToStart", beforeTime.ToString());
            }
        }

        private void chbAutoTime_Checked(object sender, RoutedEventArgs e)
        {
            SettingsHelper.setValue("autoTime", "1");
        }

        private void chbAutoTime_Unchecked(object sender, RoutedEventArgs e)
        {
            SettingsHelper.setValue("autoTime", "0");
        }

        private void deleteSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (deleteSlider != null)
            {
                int days = (int)(deleteSlider.Value);
                if (days == 1)
                    deleteTextBlock.Text = days.ToString() + " dzień";
                else
                    deleteTextBlock.Text = days.ToString() + " dni";
                SettingsHelper.setValue("numOfDays", days.ToString());
            }
        }

        private void chbDeleteAuto_Checked(object sender, RoutedEventArgs e)
        {
            SettingsHelper.setValue("autoDelete", "1");
        }

        private void chbDeleteAuto_Unchecked(object sender, RoutedEventArgs e)
        {
            SettingsHelper.setValue("autoDelete", "0");
        }

        private void chbShow_Checked(object sender, RoutedEventArgs e)
        {
            SettingsHelper.setValue("notAll", "1");
        }

        private void chbShow_Unchecked(object sender, RoutedEventArgs e)
        {
            SettingsHelper.setValue("notAll", "0");
        }
       
    }
}