﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using ProgramTV.Utility;
using System.Threading;

namespace ProgramTV.Pages
{
    public partial class Download : PhoneApplicationPage
    {
        public Download()
        {
            InitializeComponent();       
        }

        public bool ShowProgress
        {
            get { return (bool)GetValue(ShowProgressProperty); }
            set { SetValue(ShowProgressProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowProgress.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowProgressProperty =
            DependencyProperty.Register("ShowProgress", typeof(bool), typeof(Download), new PropertyMetadata(false));


        private void ButtonDownloadProgram_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.ButtonDownloadProgram.IsEnabled = false;
                string date = string.Format("{0:yyyy-MM-dd}", datePicker.Value);
                DownloaderHelper dh = new DownloaderHelper();

                ThreadPool.QueueUserWorkItem(
                   (o) =>
                   {
                       this.Dispatcher.BeginInvoke(new Action(() =>
                       {
                           ShowProgress = true;
                       }));

                       //Thread.Sleep(TimeSpan.FromSeconds(5));
                       try
                       {
                           dh.GetFile(date);
                       }
                       catch (Exception ex)
                       {
                           this.Dispatcher.BeginInvoke(() =>
                               {
                                   MessageBox.Show(ex.Message, "Błąd.", MessageBoxButton.OK);
                               });
                       }
                       finally
                       {
                           this.Dispatcher.BeginInvoke(
                               () =>
                               {
                                   ShowProgress = false;
                               });
                           this.Dispatcher.BeginInvoke(() =>
                               {
                                   MessageBox.Show("Pobrano program.");
                               });
                       }
                   });
            }
            catch (Exception ex)
            {
                MessageBox.Show("Wystąpił błąd podczas pobierania programu.\n\n" + ex.Message, "Błąd.", MessageBoxButton.OK);
            }
            finally
            {
                this.ButtonDownloadProgram.IsEnabled = true;
            }
        }
    }
}