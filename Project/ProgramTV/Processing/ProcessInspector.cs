﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Linq;
using System.Collections.Generic;
using ProgramTV.Entity;

namespace ProgramTV
{
    public sealed class ProcessInspector
    {
        private static volatile ProcessInspector instance;
        private static object syncRoot = new Object();

        private ProcessInspector() { }

        public static ProcessInspector Instance
        {
            get 
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new ProcessInspector();
                    }
                }

                return instance;
            }
        }

        public string ActualDate { get; set; }
        public Station ActualStation { get; set; }

        public ObservableCollection<Station> Stations { get; set; }
        public ObservableCollection<Program> Programs { get; set; }
        public ObservableCollection<ProgramReminder> ProgramsReminder { get; set; }

        public ProgramReminder getProgramReminderByName(string id)
        {
            int idint = int.Parse(id);
            return (from x in ProgramsReminder where x.ID == idint select x).FirstOrDefault();
        }

        public Program getProgramByName(string programName,DateTime date)
        {
            return (from x in Programs where x.Name == programName && x.Hour == date select x).FirstOrDefault();
        }

        public void setCategories()
        {
            if (ActualCategories != null && ActualCategories.Count != 0) ActualCategories.Clear();
            ActualCategories = (from x in Programs select x.Category).Distinct().ToList();         
            ActualCategories.Add("Cały program");
        }

        public List<string> ActualCategories { get; set; }

        
    }
}
