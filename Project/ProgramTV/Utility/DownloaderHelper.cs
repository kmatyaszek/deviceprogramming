﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.BackgroundTransfer;
using System.IO.IsolatedStorage;

namespace ProgramTV.Utility
{
    public class DownloaderHelper : DependencyObject
    {
        BackgroundTransferRequest transferRequest;
     
        Uri downloadUri;

        string filename, filename2;

        public void GetFile(string date)
        {
            try
            {
                string transferFileName = SettingsHelper.getValue("server") + date;
                filename = date + ".zip";
                filename2 = date;

                if (CheckNetworkAvailability() == true)
                {
                    if (transferFileName != string.Empty)
                    {
                        Uri transferUri = new Uri(Uri.EscapeUriString(transferFileName), UriKind.RelativeOrAbsolute);
                        transferRequest = new BackgroundTransferRequest(transferUri);
                        transferRequest.Method = "GET";

                        string downloadFile = transferFileName.Substring(transferFileName.LastIndexOf("/") + 1);

                        // z PathConfig
                        downloadUri = new Uri("\\shared\\transfers\\" + date + ".zip", UriKind.RelativeOrAbsolute);
                        transferRequest.DownloadLocation = downloadUri;

                        transferRequest.TransferPreferences = TransferPreferences.AllowCellularAndBattery;

                        try
                        {
                            BackgroundTransferService.Add(transferRequest);
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Nie można dodać transferu.", ex);
                        }

                        // Bind event handlers to the progess and status changed events
                        transferRequest.TransferProgressChanged +=
                            new EventHandler<BackgroundTransferEventArgs>(
                                request_TransferProgressChanged);

                        transferRequest.TransferStatusChanged +=
                            new EventHandler<BackgroundTransferEventArgs>(
                                request_TransferStatusChanged);

                        // Turn off the button for now
                        //fetchFileButton.IsEnabled = false;
                    }
                    else
                    {
                        throw new Exception("Nie podano nazwy pliku do pobrania.");
                    }
                }
                else
                {
                    throw new Exception("Nie można nawiązać połączenia z serwerem.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem z transferem.\n\n" + ex.Message, "Błąd.", MessageBoxButton.OK);
            }
        }
        
        public static bool CheckNetworkAvailability()
        {
            try
            {
                return Microsoft.Phone.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable();
            }
            catch
            {
                return false;
            }
        }

        public void request_TransferStatusChanged(object sender, BackgroundTransferEventArgs e)
        {           
            if(e.Request.StatusCode != 404) 
            {
                switch (e.Request.TransferStatus)
                {
                    case TransferStatus.Unknown:
                        throw new Exception("Wystąpił błąd podczas pobierania programu.");

                    case TransferStatus.None:
                        throw new Exception("Problem podczas pobierania programu.");

                    case TransferStatus.Paused:
                        throw new Exception("Problem podczas pobierania programu.");

                    case TransferStatus.Completed:
                        // If the status code of a completed transfer is 200 or 206, the
                        // transfer was successful
                    
                        if (transferRequest.StatusCode == 200 || transferRequest.StatusCode == 206)
                        {
                            //statusTextBlock.Text = "Completed";

                            using (IsolatedStorageFile isoStore = IsolatedStorageFile.GetUserStoreForApplication())
                            {
                                if (isoStore.FileExists(PathConfig.PathDownload + "\\" + filename))
                                {
                                    isoStore.DeleteFile(PathConfig.PathDownload + "\\" + filename);
                                }
                                isoStore.MoveFile(transferRequest.DownloadLocation.OriginalString, filename);
                            }

                           
                           // MessageBox.Show(PathConfig.getFilesFromDownloadFolder());

                            CompressHelper.UnZip(filename, filename2);
                            
                            //App.ViewModel.Items =                       
                            //    XMLStationHelper.getStationsViewModel(System.IO.Path.GetFileNameWithoutExtension(filename));

                            //ZipHelper zipHelper = new ZipHelper();
                            //zipHelper.UnzipFile(filename);

                            // Fetch the file from isolated storage and show it
                            //DisplayImage(downloadUri.OriginalString);
                        }
                        else
                        {
                            throw new Exception("Błąd: " + transferRequest.StatusCode);
                            //statusTextBlock.Text = "Error:" + transferRequest.StatusCode;
                        }

                        // Remove the transfer request in order to make room in the 
                        // queue for more transfers. Transfers are not automatically
                        // removed by the system.
                        try
                        {
                            BackgroundTransferService.Remove(transferRequest);
                        }
                        catch
                        {
                        }
                      
                        // enable the button to make more transfers possible
                        //fetchFileButton.IsEnabled = true;
                        break;

                }
            }
            else
            {
                MessageBox.Show("Nie udało się ściągnąć programu.");
            }
        }
               
        public void request_TransferProgressChanged(object sender, BackgroundTransferEventArgs e)
        {
            //Progress = ((e.Request.BytesReceived * 100.0) / e.Request.TotalBytesToReceive);
            
            //statusTextBlock.Text = e.Request.BytesReceived + " received.";
        }
    }
}
