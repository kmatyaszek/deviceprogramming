﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace ProgramTV.Pages
{
    public partial class ReminderHistory : PhoneApplicationPage
    {
        public ReminderHistory()
        {
            InitializeComponent();
            try
            {
                Utility.PathConfig.DeserializeData();
                lbPrograms.ItemsSource = ProcessInspector.Instance.ProgramsReminder;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem z załadowaniem historycznych danych.\n\n", ex.Message, MessageBoxButton.OK);
            }
        }

        private void ClearHistoryButton_Click(object sender, EventArgs e)
        {
            try
            {
                Utility.PathConfig.DeleteHistoryFile();
                ProcessInspector.Instance.ProgramsReminder = null;
                lbPrograms.ItemsSource = ProcessInspector.Instance.ProgramsReminder;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem z usunięciem historii.\n\n" + ex.Message, "Błąd.", MessageBoxButton.OK);
            }
        }
    }
}