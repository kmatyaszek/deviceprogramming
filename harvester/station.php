<?php
require "lib/XmlSerializer.php";

header('Content-Type: application/json');

$programs = json_decode(base64_decode($_POST['programs']), true);
$id = $_POST['id'];
$date = $_POST['date'];

if (is_numeric($id) && $id > 0)
{
    $folder = "data/" . $date;
    if (!is_dir($folder))
    {
        umask(0000);
        mkdir($folder, 0777);
    }
    $fp = fopen("$folder/$id.xml", "w");
    $output = XMLSerializer::generateValidXmlFromArray($programs, 'programs', 'program', array('short', 'full'));
    fwrite($fp, $output);
    fclose($fp);
    echo json_encode(array('success' => true));
}
else
{
    echo json_encode(array('success' => false));
}

