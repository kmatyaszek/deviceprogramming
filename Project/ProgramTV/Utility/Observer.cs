﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProgramTV
{
    public interface Observer
    {
        void notify(string message, int code);

        void notify(string message, int code, Object data);
    }
}
